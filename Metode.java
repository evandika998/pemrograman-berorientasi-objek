import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.*;
abstract class Metode {
    private String no_hp;
    private String password;

    public String getNo_hp() {
        return no_hp;
    }

    public void setNo_hp(String no_hp) {
        this.no_hp = no_hp;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    abstract void show();
}

class Dana extends Metode {
    @Override
    public void show() {
        setNo_hp(JOptionPane.showInputDialog(null, "Masukkan nomor telepon Anda (untuk Dana):"));
        JOptionPane.showMessageDialog(null, "Nomor telepon Anda: " + getNo_hp());
    }
}

class Ovo extends Metode {
    @Override
    public void show() {
        setNo_hp(JOptionPane.showInputDialog(null, "Masukkan nomor telepon Anda (untuk OVO):"));
        JOptionPane.showMessageDialog(null, "Nomor telepon Anda: " + getNo_hp());
    }

}
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.*;
class Pembelian {
    double dana;
    double ovo;
    double totalpulsa;
    double totaldata;

    public Pembelian(double ovo, double dana, double totalpulsa,
            double totaldata) {
        this.dana = dana;
        this.ovo = ovo;
        this.totalpulsa = totalpulsa;
        this.totaldata = totaldata;
    }

    public double getDana() {
        return dana;
    }

    public double getOvo() {
        return ovo;
    }

    public double getTotalpulsa() {
        return totalpulsa;
    }

    public double getTotaldata() {
        return totaldata;
    }

    public void beliPulsa() {
        String[] nominal = { "10.000", "25.000", "50.000", "100.000" };
        Object pulsaObj = JOptionPane.showInputDialog(null, "Pilih nominal pulsa:", "Beli Pulsa",
                JOptionPane.QUESTION_MESSAGE, null, nominal, nominal[0]);
        String pulsaStr = pulsaObj.toString().replaceAll("[^0-9]", ""); // Extract the numerical part from the selected
                                                                        // pulsa
        double pulsa = Double.parseDouble(pulsaStr);

        double dana = 500000; // Example value for available funds (dana)
        double ovo = 250000; // Example value for available OVO credit

        if (pulsa <= dana) {
            JOptionPane.showMessageDialog(null, "Pulsa " + pulsa + " berhasil dibeli!");
            dana -= pulsa;
            totalpulsa += pulsa; // Update the totalpulsa value
        } else if (pulsa <= ovo) {
            JOptionPane.showMessageDialog(null, "Pulsa " + pulsa + " berhasil dibeli!");
            ovo -= pulsa;
            totalpulsa += pulsa; // Update the totalpulsa value
        } else {
            JOptionPane.showMessageDialog(null, "Maaf, dana atau OVO tidak mencukupi.");
        }
    }

    public void belikuota() {
        String[] nominal = { "1 GB", "2 GB", "5 GB", "10 GB" };
        Object kuotaobj = JOptionPane.showInputDialog(null, "Pilih nominal Kuota:", "Beli Kuota",
                JOptionPane.QUESTION_MESSAGE, null, nominal, nominal[0]);
        String kuotastr = kuotaobj.toString().replaceAll("[^0-9]", ""); // Extract the numerical part from the selected
                                                                        // pulsa
        double Kuota = Double.parseDouble(kuotastr);
        int dana = 500000; // Example value for available funds (dana)
        int ovo = 250000; // Example value for available OVO credit

        if (Kuota <= dana) {
            JOptionPane.showMessageDialog(null, "Kuota " + Kuota + " berhasil dibeli!");
            dana -= Kuota;
            totaldata += Kuota;
        } else if (Kuota <= ovo) {
            JOptionPane.showMessageDialog(null, "Kuota " + Kuota + " berhasil dibeli!");
            ovo -= Kuota;
            totaldata += Kuota;
        } else {
            JOptionPane.showMessageDialog(null, "Maaf, dana atau OVO tidak mencukupi.");
        }
    }

    public void sisa() {
        JFrame frame = new JFrame("Sisa Pemakaian");
        frame.setSize(400, 300);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        JPanel panel = new JPanel();
        panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));

        JLabel label = new JLabel("===============================");
        JLabel title = new JLabel("SELAMAT DATANG DI TELKOMSEL");
        JLabel options = new JLabel("===============================");
        JLabel option1 = new JLabel("1. Sisa Pulsa" + totalpulsa);
        JLabel option2 = new JLabel("2. Sisa Data" + totaldata);

        panel.add(label);
        panel.add(title);
        panel.add(options);
        panel.add(option1);
        panel.add(option2);

        frame.getContentPane().add(panel);
        frame.setVisible(true);
    }
}
## 1. Mampu menunjukkan keseluruhan Use Case beserta ranking dari tiap Use Case dari produk digital:
# A. Use case user
| nomor | pengguna | use case | prioritas |
| - | ------ | ------ | ------ |
| 1 |   User     |    membeli pulsa✅    |    100    |
| 2 |   User     |    membeli data✅    |    100    |
| 3 |   User     |    mengecek sisa pulsa✅    |    60    |
| 4 |    User    |    mengecek sisa paket data✅    |    60    |
| 5 |    User    |    login✅    |    35    |
| 6 |    User    |    registrasi✅    |    40    |
| 7 |    user    |    Memasukkan nickname✅    |    5    |
| 8 |    User    |    memperoleh poin dari setiap✅ aktivitas    |    30    |
| 9 |     User   |    Check-in harian    |    15    |
| 10 |     User   |    melihat riwayat    |    40    |
| 11 |   User     |    menambah nomor    |    10    |
| 12 |   User     |    mengirim hadiah    |    25    |
| 13 |    User    |    memberi masukan✅    |    65    |
| 14 |    User    |    Menghubungi customer service    |    80    |
| 15 |    User    |    menukar poin    |    20    |
| 16 |    User    |    melihat masa aktif  nomor   |    80    |
| 17 |    user    |    pindah ke kartu lain sesama provider    |    55    |
| 18 |    User    |    melihat ping internet    |    35    |
| 19 |    User    |    mengganti bahasa    |    25    |
| 20 |    User    |    melakukan pembayaran via pulsa✅    |    100    |
| 21 |   User     |    membeli pulsa✅    |    100    |
| 22 |   User     |    membeli data✅    |    100    |
| 23 |    User    |    berlangganan    |    15    |
| 24 |    User    |    melakukan paylater    |    75    |
| 25 |    User    |    paket roaming    |    90    |
| 26 |    User    |    aktivasi 5G    |    85    |
| 27 |    User    |    membeli paket 5G    |    90    |
| 28 |    User    |    membayar token listrik    |    45    |
| 29 |    User    |    membayar indihome    |    50    |
| 30 |    User    |    melihat lokasi Grapari    |    5    |
| 31 |   User     |    mengunjungi Grapari Online    |    25    |
| 32 |   User     |    pembayaran melalui e-wallet✅    |    100    |
| 33 |    User    |    pembayaran melalui e-banking    |    85    |
| 34 |    User    |    mengunci pulsa untuk transaksi    |    60    |
| 35 |    User    |    transaksi dengan voucher    |  40      |
| 36 |   User     |    Menghubungkan dengan e-wallet✅    |     85   |
| 37 |    User    |   Menghubungkan dengan e-banking     |    80    |
| 38 |    User    |    melihat saldo e-wallet✅    |    60    |
| 39 |    User    |    melihat sisa saldo e-banking    |    60    |
| 40 |     User   |    melihat iklan    |    50    |
| 41 |   User     |    pembayaran Qris    |    85    |
| 42 |   User     |    membeli data    |    100    |
| 43 |    User    |    melihat status prabayar    |    20    |
| 44 |    User    |     memberi nilai✅   |    10    |
| 45 |     User   |    membeli paket gaming    |    100    |
| 46 |    User    |     membeli paket pendidikan   |    100    |
| 47 |    User    |    membeli paket sosial media    |   100     |
| 48 |    User    |    mengaktifkan NSP    |    65    |
| 49 |    User    |    menelusuri lagu NSP terpopuler    |    40    |
| 50 |    User    |    membeli paket kesehatan    |    100    |
| 51 |   User     |    membeli paket darurat    |    100    |
| 52 |   User     |    membeli notifikasi telkomsel✅    |    100    |
| 53 |    User    |    membeli paket bulanan    |    100    |
| 54 |    User    |    membeli paket mingguan    |    100    |
| 55 |    user    |     membeli paket harian   |    100    |
| 56 |    User    |    membeli paket malam    |    100    |
| 57 |    User    |    mengikuti undian    |    25    |
| 58 |     User   |    membeli produk    |    65    |
| 59 |     User   |    membeli paket telepon    |    100    |
| 60 |    User    |    membeli paket SMS    |    100    |
| 61 |   User     |    berlangganan modem orbit    |    85    |
| 62 |   User     |    mengajukan keluhan    |    70    |
| 63 |    User    |    berlangganan aplikasi streaming    |    65    |
| 64 |    User    |    menyimpan video di CloudMAX    |    40    |
| 65 |    User    |    menyimpan foto di CloudMAX    |    40    |
| 66 |     User   |    menyimpan dokumen di CloudMAX    |    40    |
| 67 |    User    |    menjelajahi video pendek    |    20    |
| 68 |    User    |    menonton film    |    20    |
| 69 |     User   |    mengubah suara ketika telepon    |    25    |
| 70 |     User   |    menambah backssound ketika telepon    |    25    |
| 71 |   User     |    melakukan collect call    |    10    |
| 72 |   User     |    membeli paket streaming    |    85    |
| 73 |    User    |    dapat berdonasi    |    30    |
| 74 |    User    |    melihat voucher terpopuler    |    15    |
| 75 |    User    |    melihat voucher terdekat    |    15    |
| 76 |    User    |    melihat voucher yang direkomendasikan    |    15    |
| 77 |    User    |    membeli paket khusus e-wallet    |    70    |
| 78 |    User    |    mengaktifkan layanan volte    |    80    |
| 79 |    User    |    melihat lokasi 5G    |    15    |
| 80 |    User    |    membeli kuota keluarga    |    85    |
| 81 |   User     |    membeli paket perpanjang masa    |    95    |
| 82 |   User     |    mengikuti asuransi    |    25    |
| 83 |    User    |    mengikuti bootcamp    |    25    |
| 84 |     User   |    mengikuti kuis    |    25    |
| 85 |    User    |    mengikuti survey    |    85    |
| 86 |    User    |    membayarkan tagihan    |    85    |
| 87 |    User    |    membelikan pulsa    |    85    |
| 88 |    User    |    membelikan paket data    |    85    |
| 89 |     User   |    melihat rincian voucher    |   25     |
| 90 |    User    |    melihat penawaran spesial berdasarkan langganan    |    45    |
| 91 |   User     |    mengatur parameter dalam pembelian paket    |    25    |
| 92 |   User     |    menggunakan filter untuk rincian produk    |    30   |

# B. Use case manajemen perusahaan dan direksi perusahaan
| nomor | pengguna | use case | prioritas |
| - | ------ | ------ | ------ |
| 93 |    Admin    |    melihat data pengguna    |    65    |
| 94 |    Admin    |    mengolah data    |    90    |
| 95 |    Admin    |    melihat ulasan    |    85    |
| 96 |    Admin    |    memblokir pengguna    |    75    |
| 97 |    Admin    |    memverifikasi data pengguna    |    85    |
| 98 |    Admin    |    mengawasi pengguna    |    65    |
| 99 |    Admin    |    membuat keamanan    |    100    |
| 100 |    Admin    |    menampung data    |    100    |
|101 | Admin        | Menghapus Pengguna | 75 |
## 2. Mampu Class Diagram dari keseluruhan Use Case produk digital
```mermaid 
classDiagram
    class Telkomsel {
        + menu()
    }

    class Info {
        - totalpulsa: double
        - totaldata: double
        + Info(totalpulsa: double, totaldata: double)
        + menu()
        + promo()
        + registrasi()
    }

    class Pembelian {
        - dana: double
        - ovo: double
        - hargaPulsa: double
        - hargaPaketData: double
        - totalpulsa: double
        - totaldata: double
        + Pembelian(ovo: double, dana: double, hargaPulsa: double, hargaPaketData: double, totalpulsa: double, totaldata: double)
        + PulsaDana(jumlah: double)
        + PulsaOvo(jumlah: double)
        + DataDana(jumlah: double)
        + DataOvo(jumlah: double)
        + datapulsa(jumlah: double)
        + info()
    }
    class Bonus {
        - poin: double
        + Bonus(poin: double)
        + tambahPoin()
        + promo()
        + lihatPoin()
    }

    class Metode {
        - nomor: String
        - sandi: String
        + setnomor(nomor: String)
        + setsandi(sandi: String)
        + getnomor(): String
        + getsandi(): String
    }

    class Dana {
        + masukkannomor(nomor: String)
        + masukkansandi(sandi: String)
    }

    class Ovo {
        + masukkannomor(nomor: String)
        + masukkansandi(sandi: String)
    }

    class TelkomselGUI {
        + main(args: String[])
    }

    Telkomsel --> Info
    Info --> Pembelian
    Pembelian o-- Metode
    Pembelian o-- Bonus
    Metode <|-- Dana
    Metode <|-- Ovo
```

## 3. Mampu menunjukkan dan menjelaskan penerapan setiap poin dari SOLID Design Principle
## 4. Mampu menunjukkan dan menjelaskan Design Pattern yang dipilih
## 5. Mampu menunjukkan dan menjelaskan konektivitas ke database
- Berdasarkan kasus tersebut, dengan TelkomselGUI, saya memaparakan penjelasan konektivitas antara database menggunakan PHPMyAdmin dengan bahasa pemrograman java, pertama-tama dengan menggunakan dev mysql untuk penghubungan secara otomatisnya, aplikasi tersebut dapat diunduh di [sini](https://dev.mysql.com/downloads/conector/j/)
- Selanjutnya dengan visual studio code, buatlah file koneksi.java untuk menghubungkan database tersebut
```java
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class koneksi {
    private static final String DB_URL = "jdbc:mysql://localhost:3306/telkomsel_data";
    private static final String USERNAME = "root";
    private static final String PASSWORD = "";

    public static Connection getConnection() throws SQLException {
        return DriverManager.getConnection(DB_URL, USERNAME, PASSWORD);
    }
}
```
- Dalam GUI tersebut, terdapat juga setiap operasi CRUD dalam mengoperasikan database tersebut, di antaranya:
- Create (bertujuan untuk memasukkan data ke dalam database, dalam kasus ini menggunakan contoh Registrasi):
- Update (untuk memperbarui data, dimasukkan ke dalam Registrasi):
```java
public void registrasi() {
        JOptionPane.showMessageDialog(null, "Selamat datang di Aplikasi Telkomsel");

        String nama = JOptionPane.showInputDialog("Nickname: ");
        String nomor_telepon = JOptionPane.showInputDialog("Masukkan nomor telepon:");
        String email = JOptionPane.showInputDialog("Email: ");
        String kataSandi = JOptionPane.showInputDialog("Kata sandi: ");
        String ulangiKataSandi = JOptionPane.showInputDialog("Ulangi kata sandi: ");

        if (kataSandi.equals(ulangiKataSandi)) {
            simpanDataRegistrasi(nama, nomor_telepon, email, kataSandi);
            JOptionPane.showMessageDialog(null, "Registrasi berhasil!");
            JOptionPane.showMessageDialog(null, "Terima kasih " + nama + " telah bergabung dengan Aplikasi Telkomsel.");
        } else {
            JOptionPane.showMessageDialog(null, "Kata sandi yang Anda masukkan tidak sama.");
            System.exit(0);
        }
    }

    private void simpanDataRegistrasi(String nama, String nomorTelepon, String email, String kataSandi) {
        try {
            Connection conn = koneksi.getConnection();
            String query = "INSERT INTO pengguna (Nama, Nomor, Email, Sandi) VALUES (?, ?, ?, ?)";
            PreparedStatement statement = conn.prepareStatement(query);
            statement.setString(1, nama);
            statement.setString(2, nomorTelepon);
            statement.setString(3, email);
            statement.setString(4, kataSandi);
            int k = statement.executeUpdate();
            if (k == 1)
            {
                JOptionPane.showMessageDialog(null,"Berhasil menyimpan data registrasi");
            }
            else
            {
                JOptionPane.showMessageDialog(null,"Gagal");
            }
            conn.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
```
- Read (untuk membaca database tersebut, dalam kasus ini akan dimasukkan ke dalam login untuk mengecek apakah nickname dan password sama dengan database):
```java
public void login() {
        JOptionPane.showMessageDialog(null, "Selamat datang di Aplikasi Telkomsel");

        String nama = JOptionPane.showInputDialog("Nickname: ");
        String kataSandi = JOptionPane.showInputDialog("Kata sandi: ");

        if (validateLogin(nama, kataSandi)) {
            JOptionPane.showMessageDialog(null, "Login berhasil");
            JOptionPane.showMessageDialog(null, "Terima kasih " + nama + " telah bergabung dengan Aplikasi Telkomsel.");
        } else {
            JOptionPane.showMessageDialog(null, "Login gagal. Periksa kembali nama pengguna dan kata sandi Anda.");
        }
        int ok=JOptionPane.showConfirmDialog(null,"Apakah Yakin Mennghapus Data???", "Confirmation",JOptionPane.YES_NO_CANCEL_OPTION);
        if (ok==0)
        {
            hapusdata(nama);
        }
    }

    private boolean validateLogin(String nama, String kataSandi) {
        try {
            Connection conn = koneksi.getConnection();
            String query = "SELECT * FROM pengguna WHERE Nama=? AND Sandi=?";
            PreparedStatement statement = conn.prepareStatement(query);
            statement.setString(1, nama);
            statement.setString(2, kataSandi);
            ResultSet resultSet = statement.executeQuery();
            if (resultSet.next()) {
                return true;
            }
            conn.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

```
- Delete (untuk menghapus index dalam database, dalam contoh ini akan digunakan untuk menghapus data pengguna):
```java
public void hapusdata(String nama)
    {
        try
         {
            Connection conn = koneksi.getConnection();
            String sql = "DELETE FROM pengguna WHERE Nama = ?";
            PreparedStatement statement = conn.prepareStatement(sql);
            statement.setString(1, nama);
            int rowsDeleted = statement.executeUpdate();
            JOptionPane.showMessageDialog(null, "Hapus Nama Berhasil");
         }
        catch (Exception e)
        {
            JOptionPane.showMessageDialog(null, "Hapus Nama Gagal");
        }
    }
```


## 6. Mampu menunjukkan dan menjelaskan pembuatan web service dan setiap operasi CRUD nya
## 7. Mampu menunjukkan dan menjelaskan Graphical User Interface dari produk digital
### Pada Telkomsel demo yang ditunjukkan, terdapat beberapa use case yang di realisasikan melalui GUI java, yaitu:
- Tampilan Utama, berisi dua pilihan berupa login dan registrasi
![awal](https://gitlab.com/evandika998/pemrograman-berorientasi-objek/-/raw/main/1.png)
- Tampilan Login, berisi nickname dan password
![login](https://gitlab.com/evandika998/pemrograman-berorientasi-objek/-/raw/main/3.png)
- Konfirmasi hapus data, memungkinkan jika si pengguna ingin menghapus datanya
![hapus](https://gitlab.com/evandika998/pemrograman-berorientasi-objek/-/raw/main/5.png)
- Menu, berisi pilihan yang akan dipilih oleh user
![Menu](https://gitlab.com/evandika998/pemrograman-berorientasi-objek/-/raw/main/6.png)
- Opsi pembayaran, berisi pilihan pembayaran ketika melakukan pembayaran produk
![opsi](https://gitlab.com/evandika998/pemrograman-berorientasi-objek/-/raw/main/7.png)
- Nominal, berisi nominal pembayaran yang ingin dibayar
![nominal](https://gitlab.com/evandika998/pemrograman-berorientasi-objek/-/raw/main/8.png)
- Riwayat, berisi riwayat pembelian dan total pulsa/data
![Riwayat](https://gitlab.com/evandika998/pemrograman-berorientasi-objek/-/raw/main/9.png)
## 8. Mampu menunjukkan dan menjelaskan HTTP connection melalui GUI produk digital
## 9. Mampu Mendemonstrsikan produk digitalnya kepada publik dengan cara-cara kreatif melalui video Youtube
## 10.BONUS !!! Mendemonstrasikan penggunaan Machine Learning

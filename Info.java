import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.*;
class Info extends Telkomsel {
    private double totalpulsa;
    private double totaldata;

    public Info(double totalpulsa, double totaldata) {
        this.totalpulsa = totalpulsa;
        this.totaldata = totaldata;
    }

    public void menu() {
        JFrame frame = new JFrame("Telkomsel Menu");
        frame.setSize(400, 300);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        JPanel panel = new JPanel();
        panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));

        JLabel label = new JLabel("===============================");
        JLabel title = new JLabel("SELAMAT DATANG DI TELKOMSEL");
        JLabel options = new JLabel("===============================");
        JLabel option1 = new JLabel("Sisa Pulsa : " + totalpulsa);
        JLabel option2 = new JLabel("Total Kuota : " + totaldata);

        panel.add(label);
        panel.add(title);
        panel.add(options);
        panel.add(option1);
        panel.add(option2);

        frame.getContentPane().add(panel);
        frame.setVisible(true);
    }

    public void registrasi() {
        JOptionPane.showMessageDialog(null, "Selamat datang di Aplikasi Telkomsel");

        String nama = JOptionPane.showInputDialog("Nickname: ");
        String nomor_telepon = JOptionPane.showInputDialog("Masukkan nomor telepon:");
        String email = JOptionPane.showInputDialog("Email: ");
        String kataSandi = JOptionPane.showInputDialog("Kata sandi: ");
        String ulangiKataSandi = JOptionPane.showInputDialog("Ulangi kata sandi: ");

        if (kataSandi.equals(ulangiKataSandi)) {
            simpanDataRegistrasi(nama, nomor_telepon, email, kataSandi);
            JOptionPane.showMessageDialog(null, "Registrasi berhasil!");
            JOptionPane.showMessageDialog(null, "Terima kasih " + nama + " telah bergabung dengan Aplikasi Telkomsel.");
        } else {
            JOptionPane.showMessageDialog(null, "Kata sandi yang Anda masukkan tidak sama.");
            System.exit(0);
        }
    }

    private void simpanDataRegistrasi(String nama, String nomorTelepon, String email, String kataSandi) {
        try {
            Connection conn = koneksi.getConnection();
            String query = "INSERT INTO pengguna (Nama, Nomor, Email, Sandi) VALUES (?, ?, ?, ?)";
            PreparedStatement statement = conn.prepareStatement(query);
            statement.setString(1, nama);
            statement.setString(2, nomorTelepon);
            statement.setString(3, email);
            statement.setString(4, kataSandi);
            int k = statement.executeUpdate();
            if (k == 1)
            {
                JOptionPane.showMessageDialog(null,"Berhasil menyimpan data registrasi");
            }
            else
            {
                JOptionPane.showMessageDialog(null,"Gagal");
            }
            conn.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void login() {
        JOptionPane.showMessageDialog(null, "Selamat datang di Aplikasi Telkomsel");

        String nama = JOptionPane.showInputDialog("Nickname: ");
        String kataSandi = JOptionPane.showInputDialog("Kata sandi: ");

        if (validateLogin(nama, kataSandi)) {
            JOptionPane.showMessageDialog(null, "Login berhasil");
            JOptionPane.showMessageDialog(null, "Terima kasih " + nama + " telah bergabung dengan Aplikasi Telkomsel.");
        } else {
            JOptionPane.showMessageDialog(null, "Login gagal. Periksa kembali nama pengguna dan kata sandi Anda.");
        }
        int ok=JOptionPane.showConfirmDialog(null,"Apakah Yakin Mennghapus Data???", "Confirmation",JOptionPane.YES_NO_CANCEL_OPTION);
        if (ok==0)
        {
            hapusdata(nama);
        }
    }

    private boolean validateLogin(String nama, String kataSandi) {
        try {
            Connection conn = koneksi.getConnection();
            String query = "SELECT * FROM pengguna WHERE Nama=? AND Sandi=?";
            PreparedStatement statement = conn.prepareStatement(query);
            statement.setString(1, nama);
            statement.setString(2, kataSandi);
            ResultSet resultSet = statement.executeQuery();
            if (resultSet.next()) {
                return true;
            }
            conn.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    public void kritikSaran() {
        String saran = JOptionPane.showInputDialog(null, "Masukkan saran : ");
        String input = JOptionPane.showInputDialog(null, "Masukkan penilaian (berupa bilangan bulat):");
        try {
            int angka = Integer.parseInt(input);
            JOptionPane.showMessageDialog(null, "penilaian: " + angka);
        } catch (NumberFormatException e) {
            JOptionPane.showMessageDialog(null, "Input yang dimasukkan bukan bilangan bulat!", "Error",
                    JOptionPane.ERROR_MESSAGE);
        }

    }
    public void hapusdata(String nama)
    {
        try
         {
            Connection conn = koneksi.getConnection();
            String sql = "DELETE FROM pengguna WHERE Nama = ?";
            PreparedStatement statement = conn.prepareStatement(sql);
            statement.setString(1, nama);
            int rowsDeleted = statement.executeUpdate();
            JOptionPane.showMessageDialog(null, "Hapus Nama Berhasil");
         }
        catch (Exception e)
        {
            JOptionPane.showMessageDialog(null, "Hapus Nama Gagal");
        }
    }
}
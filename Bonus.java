import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.*;
class Bonus {
    private double poin;
    private JFrame frame;

    public Bonus(double poin) {
        this.poin = poin;
        createGUI();
    }

    private void createGUI() {
        frame = new JFrame("Bonus Program");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(400, 200);
        frame.setLayout(null);

        JLabel poinLabel = new JLabel("Jumlah poin: " + poin);
        poinLabel.setBounds(20, 20, 200, 30);
        frame.add(poinLabel);

        JButton tambahPoinButton = new JButton("Tambah Poin");
        tambahPoinButton.setBounds(20, 60, 120, 30);
        tambahPoinButton.addActionListener(e -> tambahPoin());
        frame.add(tambahPoinButton);

        JButton promoButton = new JButton("Promo");
        promoButton.setBounds(150, 60, 120, 30);
        promoButton.addActionListener(e -> promo());
        frame.add(promoButton);

        JButton lihatPoinButton = new JButton("Lihat Poin");
        lihatPoinButton.setBounds(280, 60, 100, 30);
        lihatPoinButton.addActionListener(e -> lihatPoin());
        frame.add(lihatPoinButton);

        frame.setVisible(true);
    }

    private void tambahPoin() {
        String input = JOptionPane.showInputDialog(frame, "Masukkan jumlah poin yang ingin ditambah:");
        try {
            double poinTambahan = Double.parseDouble(input);
            poin += poinTambahan;
            JOptionPane.showMessageDialog(frame, "Poin berhasil ditambahkan!");
        } catch (NumberFormatException e) {
            JOptionPane.showMessageDialog(frame, "Input tidak valid. Masukkan angka.");
        }
    }

    private void promo() {
        JOptionPane.showMessageDialog(frame, "Selamat! Anda mendapatkan promo spesial.");
    }

    private void lihatPoin() {
        JOptionPane.showMessageDialog(frame, "Jumlah poin Anda saat ini: " + poin);
    }
}
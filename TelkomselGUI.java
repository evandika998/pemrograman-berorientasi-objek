import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.*;


class TelkomselGUI {
    public static void main(String[] args) {
        double dana = 100000;
        double ovo = 50000;
        double totalpulsa = 10000;
        double totaldata = 15;
        Info info = new Info(totalpulsa, totaldata);
        Pembelian pembelian = new Pembelian(dana, ovo, totalpulsa, totaldata);

        String[] options = { "Registrasi", "Login" };
        int choice = JOptionPane.showOptionDialog(null, "Silakan pilih menu:", "Menu Utama", JOptionPane.DEFAULT_OPTION,
                JOptionPane.PLAIN_MESSAGE, null, options, options[0]);

        if (choice == 0) {
            info.registrasi();
        } else if (choice == 1) {
            info.login();
        }
        info.menu();
        String[] menu = { "Beli Pulsa", "Beli Kuota", "Kritik dan Saran" };
        int menuChoice = JOptionPane.showOptionDialog(null, "Silakan pilih menu:", "Menu Pembelian",
                JOptionPane.DEFAULT_OPTION, JOptionPane.PLAIN_MESSAGE, null, menu, menu[0]);
        Metode metode;
        String[] metodeOptions = { "Dana", "OVO" };

        if (menuChoice == 0) {
            int metodeChoice = JOptionPane.showOptionDialog(null, "Pilih metode pembayaran:", "Metode Pembayaran",
                    JOptionPane.DEFAULT_OPTION, JOptionPane.PLAIN_MESSAGE, null, metodeOptions, metodeOptions[0]);

            if (metodeChoice == 0) {
                metode = new Dana();
                metode.show();
                pembelian.beliPulsa();
            } else if (metodeChoice == 1) {
                metode = new Ovo();
                metode.show();
                pembelian.beliPulsa();
            }
            pembelian.sisa();
        }
        if (menuChoice == 1) {
            int metodeChoice = JOptionPane.showOptionDialog(null, "Pilih metode pembayaran:", "Metode Pembayaran",
                    JOptionPane.DEFAULT_OPTION, JOptionPane.PLAIN_MESSAGE, null, metodeOptions, metodeOptions[0]);

            if (metodeChoice == 0) {
                metode = new Dana();
                metode.show();
                pembelian.belikuota();
            } else if (metodeChoice == 1) {
                metode = new Ovo();
                metode.show();
                pembelian.belikuota();
            }
            pembelian.sisa();
        }
        if (menuChoice == 2) {
            info.kritikSaran();
        }
    }
}
